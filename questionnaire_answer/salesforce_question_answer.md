# Bluetel Salesforce Candidate Questionnaire

Thanks for taking interest in a position at Bluetel! Answering this set of questions gives us a good idea about your technical and logical capabilities; as well as a couple of personal characteristics. You can be as brief or comprehensive as you wish in your answers. We don’t expect every candidate to know all the answers to these questions, so please don’t be discouraged if you don’t know the answer! Good luck!  

### Question 1
Create an algorithm that prints the integers from 17 to 53. However for multiples of two, print "Foo" instead  of the number and for multiples of five print "Bar". For numbers which are multiples of both two and five print "FooBar". 

### Answer

```Java
for(Integer i=17; i<=53; i++){
    if(Math.Mod(i,2)==0 && Math.Mod(i,5)==0) {
        System.debug('FooBar  --> i='+i);
    } else if(Math.Mod(i,2)==0){
        System.debug('Foo  --> i='+i);
    } else if(Math.Mod(i,5)==0) {
        System.debug('Bar  --> i='+i);
    } else {
        System.debug('i='+i);
    }
}
```

### Question 2
In a relational database, why is redundant data (i.e. the same data stored in multiple tables) generally a bad thing?

### Answer

There are several negative impacts of having redundant data in RDB. The most common ones are – 
a)  inherits data corruption
b)  increases the chance of data corruption 
c)  add programming complexity
d)  takes additional spaces
e)  may provide incorrect information due to inconsistency 


### Question 3
In a relational database, why might redundant data be necessary in real world applications?

### Answer
There are various scenarios where data redundant is intentionally designed; for example – 
a)  in a distributed system, the same data may be stored in several locations to increase performance by reducing data retrieval time
b)  to provide an alternative backup data in the event of digester recovery
c)  to improve data quality, multiple copies of data is stored for cross checking purpose
d)  to increase data security from hackers or cyberattacks, data is stored in several locations


### Question 4
In development teams, multiple people are often involved in building and maintaining a single salesforce instance. They may be working on a single task together, or multiple tasks, and changes made by one developer may conflict with those of another. What system would you suggest to help manage this, and why would you choose your solution in particular?

### Answer

Various functionalities in Source control repository such as Git or Bitbucket can be used within the development team. Functionalities like creating multiple branches and merging these branched to the master branch or production deployment can enable multiple developers working in the same functionalities/files simultaneously.


### Question 5

The below Salesforce Apex code may fail on any line with a `DMLException`. Modify the code to ensure that if it were to fail then no data would be saved.
>
```Java
Database.insert(action);
Database.update(actionPlan);
Database.update(customer);
```

### Answer

The Database class methods don’t throw exceptions hence, the DML is used within try-catch block with a Savepoint. This will throw exception in the event of failure which are handled in 3 different catch block for future investigation purpose.
>
```Java
Savepoint sp = Database.setSavepoint();
try{
    insert action;
    update actionPlan;
    update customer;
} catch(DMLException ex) {
    System.debug('Error - DMLException: ' + ex);
    Database.rollback(sp);
} catch(SObjectException ex) {
    System.debug('Error - SObjectException: ' + ex);
    Database.rollback(sp);
} catch(Exception ex) {
    System.debug('Error - Exception: ' + ex);
    Database.rollback(sp);
}
```

### Question 6

What is the difference between `Trigger.old` and `Trigger.new`? Under which types of trigger are they each available?

### Answer

According to Trigger context variable - 
Trigger.old contains a list of old version of the sObject records which are only available in the event of update and delete triggers.

Trigger.new contains a list of new version of the sObject records which are only available in insert, update and undelete triggers. Records can only be modified in before triggers.


### Question 7

Provide Salesforce Apex code which uses SOQL to extract the following fields from a custom object named `Visitors` into a List variable. Only records where the value of the `Company__c` field is equal to `Bluetel` should be retrieved.

- Id
- Name
- Visit_Time__c
- Visiting__c

### Answer
>
```Java
List<Visitors__c> visitorList = [SELECT Id, Name, Visit_Time__c, Visiting__c FROM Visitors__c WHERE Company__c = 'Bluetel'];
```

Although Id field is always retrieved even when it is not mentioned in the query. But it is added in the code above for clarification purpose only. It can be removed from the code and still be referenced to if needed.


### Question 8

The following provides one test scenario for the requirement `Insurance Requests for amounts less than $100,000 should not be submitted to the insurer, and the user should be presented with an error if they attempt to make this submission`. Summarise the minimum test cases that you would define for the <u>entire</u> requirement.

> **Given** I am completing an insurance request 
>
> **When** I specify a cover amount of less than $100,000
> **AND** I choose to submit my request
>
> **THEN** I should receive an error 
> **AND** the request should not be submitted


### Answer

Boundary value testing needs to be performed for this requirement with lower and upper boundary values; the amounts are $99,999.99 and $100,000 respectively. The following 2 tests can be performed -  

1. Check that if an insurance request is submitted with a covering amount =$99,999.99 then an error is received and request is not submitted

2. Check that if an insurance request is submitted with a covering amount =$100,000 then request is submitted successfully



