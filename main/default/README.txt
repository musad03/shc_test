Please note that the solution is a Minimum Viable Product(MVP) due to the difference of individual understanding the of given tasks. There are areas of potential improvements which require more information around the sObjects and relationship among them. 

Design standard
This is designed to meet the minimum criteria which were assumed from the task description. If there were more information, the sObjects and their relationship could have been designed more appropriately.

Coding standard
The error handling can be improved to provide a better user experience by adding custom error handling mechanism within the Class or in a Helper class.

A Swagger page could have been defined for external development/testing team to refer to during development phase of the client system.

Testing Best practices
There are happy path tests which meet the Salesforce test coverage requirement; however, some negative test could have been added if error handling was added in the underlying class.



