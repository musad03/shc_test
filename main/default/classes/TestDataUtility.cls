@IsTest
public with sharing class TestDataUtility {
    
    public static List<Account> getAccounts(Integer count) {
        List<Account> records = new List<Account>();
        for(Integer i=0; i<count; i++){
            Account a = new Account(Name='Test account '+i, 
                                    BillingCity='Test Billing City',
                                    BillingState = 'Test billing state',
                                    ShippingState = 'Test billing state',
                                    BillingPostalCode = 'PS123',
                                    ShippingPostalCode = 'PS123',
                                    Active__c='Yes');
            records.add(a);
        }
        return records;
    }

    public static List<Register__c> getRegisters(List<Account> accounts) {
        List<Register__c> records = new List<Register__c>();
        Integer i=1;
        for(Account a : accounts){
            records.add(new Register__c(customerId__c=a.Id,
                                            serialNumber__c='1234567890'+ i,
                                            mpxn__c=Integer.valueOf('1234567'+i),
                                            registerId__c=Integer.valueOf('12345'+ i++),
                                            Read_DateTime__c=System.now()));
        }
        return records;
    }

    public static List<Read__c> getReads(Integer count, Register__c reg){
        List<Read__c> records = new List<Read__c>();
        
        for(Integer i=0; i<count; i++){
            records.add(new Read__c(Register__c=reg.Id,
                                    Type__c='TestReadType'+i, 
                                    registerId__c=Integer.valueOf(reg.registerId__c), 
                                    Value__c=Integer.valueOf('100'+i)));
        }
        return records;
    }

    public static List<MeterRead.Read> getMeterReadReads(Integer count){
        List<MeterRead.Read> readList = new List<MeterRead.Read>();

        for(Integer i=0; i<count; i++){
            MeterRead.Read read = new MeterRead.Read();
            read.type = 'test type'+i;
            read.registerId = '234564';
            read.value='123'+i;
            readList.add(read);
        }
        return readList;
    }
}
