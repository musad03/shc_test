@IsTest
public with sharing class MeterReadTest {
    @isTest
    static void testAddMeterReading(){
        //SETUP TEST DATA
        List<Account> accounts = TestDataUtility.getAccounts(1);
        insert accounts;
        Integer readCount = 2;
        List<MeterRead.Read> read = TestDataUtility.getMeterReadReads(readCount);

        //CHECK DATA PREP IS COMPLETED SUCCESSFULLY
        System.assert(accounts.size()>0 && read.size()>0,'TEST DATA SETUP CHECK');

        //CHEKC HAPPY PATH WITH ALL VALID INPUT
        String customerId = accounts[0].Id;
        String serialNumber = '145452324323';
        String mpxn = '45435345';
        String readDate = String.valueOf(System.now());
        Register__c register = MeterRead.addMeterRead(customerId, serialNumber, mpxn, read, readDate);
        System.assert(register != null, 'MeterRead creation');
        System.debug('register: '+register);
        System.assert(register.customerId__c == customerId && register.serialNumber__c == serialNumber, 'Register__c record value checks');
    }

    @isTest
    static void testGetMeterReadByMeterCustomerId(){
        //SETUP TEST DATA
        List<Account> accounts = TestDataUtility.getAccounts(1);
        insert accounts;
        List<Register__c> registerList = TestDataUtility.getRegisters(accounts);
        insert registerList;
        List<Read__c> reads = TestDataUtility.getReads(2, registerList[0]);
        insert reads;

        //CHECK DATA PREP IS COMPLETED SUCCESSFULLY
        System.assert(accounts.size()>0 && registerList.size()>0 && reads.size()>0,'TEST DATA SETUP CHECK');

        RestRequest request = new RestRequest();
        String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
        request.requestUri = sfdcBaseURL+'/services/apexrest/meter-read/';
        request.addParameter('customerId', accounts[0].Id);
        request.addParameter('serialNumber', registerList[0].serialNumber__c);
        request.httpMethod = 'GET';
        RestContext.request = request;

        MeterRead.CustomerMeterRead cmr = MeterRead.getMeterReadByMeterCustomerId();
        
        //CHECK RESPONSE IS NOT NULL
        System.assert(cmr != null, 'RESPONSE CHECK');
        
        //CHECK RESPONSE HAS EXPECTED VALUES
        System.assertNotEquals(String.valueOf(cmr).indexOf(accounts[0].Id), -1);
        System.assertNotEquals(String.valueOf(cmr).indexOf(registerList[0].serialNumber__c), -1);
    }

}
