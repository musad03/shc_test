@RestResource(urlMapping='/meter-read/*')
global with sharing class MeterRead {
     /**
        TO CALL THE GET METHOD A VALID CUSTOMER ID (ACCOUND ID) IS REQUIRED
        WORKBENCH GET URL  /services/apexrest/meter-read/
     */
    @HttpPost
    global static Register__c addMeterRead(  String customerId, 
                                        String serialNumber,
                                        String mpxn,
                                        List<Read> read,
                                        String readDate) {
        System.debug('GOT THE REQUEST');
        System.debug('customerId: '+customerId);
        System.debug('serialNumber: '+serialNumber);
        System.debug('mpxn: '+mpxn);
        System.debug('read: '+read);
        System.debug('readDate: '+readDate);
        System.debug('registerId__c: '+Integer.valueOf(read[0].registerId));
        
        String recordId;
        // @TODO: ADD CODE TO HANDLE THE REQUEST
        Savepoint sp = Database.setSavepoint();
        Register__c register = new Register__c( customerId__c=customerId,
                                                serialNumber__c=serialNumber,
                                                mpxn__c=Integer.valueOf(mpxn),
                                                registerId__c=Integer.valueOf(read[0].registerId),
                                                Read_DateTime__c=Datetime.valueOf(readDate.replace('T',' ')));
        insert register;

        List<Read__c> readRecords = new List<Read__c>();
        for(Read r : read){
            readRecords.add(new Read__c(Register__c=register.Id,
                                        Type__c=r.type, 
                                        registerId__c=Integer.valueOf(r.registerId), 
                                        Value__c=Integer.valueOf(r.value)));
        }
        insert readRecords;
        recordId = register.Id;
        
        Database.rollback(sp);
        System.debug('register: ' + register + '\n readRecords: ' + readRecords);
        return register;
    }

    /**
        TO CALL THE GET METHOD A VALID CUSTOMER ID (ACCOUND ID) AND SERIAL NUMBER IS REQUIRED
        WORKBENCH GET URL  /services/apexrest/meter-read?customerId=0011t00000LysFMAAZ&serialNumber=27263927192
     */
    @HttpGet
    global static CustomerMeterRead getMeterReadByMeterCustomerId(){
        RestRequest request = RestContext.request;
        System.debug('requst.requestUri: ' + request.requestURI);
        String customerId = request.params.get('customerId'); 
        String serialNumber = request.params.get('serialNumber');

        System.debug('customerId='+customerId+ ' serialNumber='+serialNumber);
        
        Register__c record = [SELECT customerId__c, serialNumber__c, mpxn__c,
                                (SELECT Type__c, registerId__c, Value__c FROM Reads__r),
                                Read_DateTime__c
                                FROM Register__c
                                WHERE customerId__c=:customerId AND serialNumber__c=:serialNumber LIMIT 1];
        System.debug('record: ' + record);
        
        CustomerMeterRead cmr = new CustomerMeterRead();
        cmr.readDate = String.valueOf(record.Read_DateTime__c);
        for(Read__c r : record.Reads__r){
            Read rd = new Read();
            rd.type = r.type__c;
            rd.registerId = String.valueOf(r.registerId__c);
            rd.value = String.valueOf(r.value__c);
            cmr.read.add(rd);
        }
        cmr.mpxn = String.valueOf(record.mpxn__c);
        cmr.serialNumber = record.serialNumber__c;
        cmr.customerId = String.valueOf(record.customerId__c).replace(' ','T');

        System.debug('CustomerMeterRead.read'+cmr.read);
        System.debug('CustomerMeterRead cmr::: '+cmr);

        return cmr;
    }

    global class Read {
        public String type;
        public String registerId;
        public String value;
    }

    global class CustomerMeterRead {
        String customerId; 
        String serialNumber;
        String mpxn;
        List<Read> read = new List<Read>();
        String readDate; 
    }
}
